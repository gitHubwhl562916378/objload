#ifndef OBJLOADER_H
#define OBJLOADER_H

#include <QVector>
#include "modelloaderi.h"
class ObjLoader : public ModelLoaderI
{
public:
    ObjLoader() = default;
    bool load(QString fileName, QVector<float> &vPoints, QVector<float> &tPoints, QVector<float> &nPoints);
};

#endif // OBJLOADER_H
